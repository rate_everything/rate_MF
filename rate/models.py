from django.db import models
from django.db.models.signals import post_save
from slugify import slugify


class IntegerRangeField(models.IntegerField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value':self.max_value}
        defaults.update(kwargs)
        return super(IntegerRangeField, self).formfield(**defaults)


class Scores(models.Model):
    GRADES = [
        (None, "N/A"),
        (0, "AA"),
        (1, "BA"),
        (2, "BB"),
        (3, "CB"),
        (4, "CC"),
        (5, "DC"),
        (6, "DD"),
        (7, "FF"),
        (8, "VF"),
    ]

    student = models.ForeignKey("Student")
    comment = models.TextField()
    overall_score = IntegerRangeField(min_value=1,max_value=10)
    difficulty_score = IntegerRangeField(min_value=1,max_value=10)
    for_credit = models.NullBooleanField(blank=True)
    attendance = models.NullBooleanField(blank=True)
    textbook_used = models.NullBooleanField(blank=True)
    would_take_again = models.NullBooleanField(blank=True)
    grade_received = models.PositiveSmallIntegerField(choices=GRADES, blank=True, null=True)
    up_voters = models.ManyToManyField("Student", related_name="+")
    down_voters = models.ManyToManyField("Student", related_name="+")
    tags = models.ManyToManyField('Tags')


class Professors(models.Model):
    name = models.CharField(max_length=200, unique=True, blank=False)
    office = models.CharField(max_length=100, blank=False)
    slug = models.CharField(max_length=200, blank=True)
    scores = models.ManyToManyField("Scores", related_name="scoreconnect")

    def __str__(self):
        return "Name : {} | Office: ".format(self.name)


def save_slug(sender, instance, **kwargs):
    Professors.objects.filter(id=instance.id).update(slug=slugify(instance.name))


post_save.connect(save_slug, sender=Professors)


class Classes(models.Model):
    code = models.CharField(max_length=10, unique=True, blank=False)
    name = models.CharField(max_length=200, unique=True, blank=False)
    credit = models.FloatField()
    professor = models.ManyToManyField('Professors')


class Tags(models.Model):
    tag = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return "{}".format(self.tag)


class Comments(models.Model):
    message = models.TextField(max_length=600, blank=False)
    created = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey('Student', blank=False, null=True)


class Student(models.Model):
    name = models.CharField(max_length=200, unique=True, blank=False)
    email = models.EmailField(unique=True, blank=False)

    def __str__(self):
        return "{}".format(self.name)
