# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-27 19:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rate', '0002_professors_slug'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='scores',
            name='professor',
        ),
        migrations.AddField(
            model_name='professors',
            name='scores',
            field=models.ManyToManyField(related_name='scoreconnect', to='rate.Scores'),
        ),
        migrations.AlterField(
            model_name='professors',
            name='slug',
            field=models.CharField(blank=True, max_length=200),
        ),
    ]
