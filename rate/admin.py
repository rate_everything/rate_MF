from django.contrib import admin
from .models import *


@admin.register(Classes)
class ClassesAdmin(admin.ModelAdmin):
    list_display = [
        'code',
        'name',
        'credit',
    ]
    search_fields = [
        'code',
        'name',
        'credit',
    ]
    list_filter = [
        'code',
        'name',
        'credit',
    ]


@admin.register(Professors)
class ProfessorsAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'office',
    ]
    search_fields = [
        'name',
        'office',
    ]
    list_filter = [
        'name',
        'office',
    ]


@admin.register(Scores)
class ScoresAdmin(admin.ModelAdmin):
    list_display = [
        'grade_received',
        'student',
        'comment',
        'overall_score',
        'difficulty_score',
        'for_credit',
        'attendance',
        'textbook_used',
        'tags_list',
        'would_take_again',
    ]
    search_fields = [
        'grade_received',
        'student',
        'comment',
        'overall_score',
        'difficulty_score',
        'for_credit',
        'attendance',
        'textbook_used',
        'tags_list',
        'would_take_again',
    ]
    list_filter = [
        'grade_received',
        'student',
        'comment',
        'overall_score',
        'difficulty_score',
        'for_credit',
        'attendance',
        'textbook_used',
        'tags',
        'would_take_again',
    ]

    def tags_list(self, obj):
        return "<br>".join(obj.tags.values_list("tag", flat=True))

    tags_list.allow_tags = True
    tags_list.short_description = 'Tags'



@admin.register(Tags)
class TagsAdmin(admin.ModelAdmin):
    list_display = [
        'tag'
    ]
    search_fields = [
        'tag'
    ]
    list_filter = [
        'tag'
    ]


@admin.register(Comments)
class CommentsAdmin(admin.ModelAdmin):
    list_display = [
        'message',
        'created',
        'author',
    ]
    search_fields = [
        'message',
        'created',
        'author',
    ]
    list_filter = [
        'message',
        'created',
        'author',
    ]


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'email',
    ]
    search_fields = [
        'name',
        'email',
    ]
    list_filter = [
        'name',
        'email',
    ]