from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name="index"),
    url(r'^professors/$', views.ProfessorsView.as_view(), name="professors"),
    url(r'^about/$', views.AboutView.as_view(), name="about"),
    url(r'^prof/(?P<slug>.*)$', views.Prof1View.as_view(), name="prof"),
    url(r'^prof_detail/$', views.Prof1View.as_view(), name="prof_detail"),
]