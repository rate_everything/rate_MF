from rate.models import Scores, Professors
from django.views import generic


class IndexView(generic.ListView):
    model = Professors
    template_name = 'rate/index.html'


class ProfessorsView(generic.ListView):
    model = Professors
    template_name = 'rate/professors.html'

    def get_queryset(self):
        if self.request.GET.get("q"):
            query = Professors.objects.filter(name__icontains=self.request.GET.get("q"))
        else:
            query = Professors.objects.all()
        return query


class Prof1View(generic.ListView):
    model = Professors
    template_name = 'rate/prof_detail.html'

    def get_queryset(self):
        query = Professors.objects.filter(slug=self.kwargs.get('slug')).get()
        return query


class Prof2View(generic.ListView):
    model = Professors
    template_name = 'rate/prof2.html'


class AboutView(generic.TemplateView):
    template_name = 'rate/about.html'




